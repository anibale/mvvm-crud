﻿using AppWPF.MVVM.Model;
using AppWPF.MVVM.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;


namespace AppWPF.ViewModel
{
    class ClientViewModel : ObservableCollection<Client>, INotifyPropertyChanged
    {

        #region Atributos
        private int selectedIndex;

        private int id;
        private string name;
        private string lastName;
        private string document;
        private string address;
        private string gender;
        //private DateTime? birthdate;
        private ICommand newClientCommand;
        private ICommand addClientCommand;
        private ICommand delClientCommand;
        private ICommand updateClientCommand;

        //OleDbProperty
        OleDbConnection conector;
        private OleDbDataReader reader;
        private OleDbTransaction tra;
        private OleDbDataAdapter adapter;

        //Conexion con la Base de Datos
        public string ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "\\Clientes.mdb";
        #endregion

        #region Propiedades
        public int SelectedIndexOfCollection
        {
            get
            {
                return selectedIndex;
            }//Fin de get.
            set
            {
                selectedIndex = value;
                OnPropertyChanged("SelectedIndexOfCollection");
                //Activa el evento OnPropertyChanged de los atributos para refrescar las propiedades segun el indice seleccionado.
                OnPropertyChanged("Id");
                OnPropertyChanged("Name");
                OnPropertyChanged("LastName");
                OnPropertyChanged("Document");
                OnPropertyChanged("Address");
                OnPropertyChanged("Gender");
                OnPropertyChanged("Birthdate");
            }//Fin de set.
        }//Fin

        private ObservableCollection<string> values = new ObservableCollection<string>()//Combobox de Genero
        {
            "Femenino", "Masculino"
        };
        public ObservableCollection<string> Values
        {
            get { return values; }
            set
            {
                values = value;
                OnPropertyChanged("Gender");
            }
        }
        private string selectedValue;
        public string SelectedValue
        {
            get { return selectedValue; }
            set
            {
                selectedValue = value;
                OnPropertyChanged("Gender");
            }
        }

        private DateTime birthdate = DateTime.Today.Date;
        public DateTime Birthdate
        {
            get
            {
                   return birthdate;
            }
            set
            {
                birthdate = value;
                OnPropertyChanged("Birthdate");

            }
        }

        public int Id
        {
            get
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    return this.Items[this.SelectedIndexOfCollection].Id;
                }//Fin de if.
                else
                {
                    return id;
                }//Fin de else.
            }//Fin de get.
            set
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    this.Items[this.SelectedIndexOfCollection].Id = value;
                }//Fin de if.
                else
                {
                    id = value;
                }//Fin de else.
                OnPropertyChanged("Id");
            }//Fin de set.
        }//Fin de propiedad Id.

        public string Name
        {
            get
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    return this.Items[this.SelectedIndexOfCollection].Name;
                }//Fin de if.
                else
                {
                    return name;
                }//Fin de else. 
            }//Fin de get.
            set
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    this.Items[this.SelectedIndexOfCollection].Name = value;
                }//Fin de if.
                else
                {
                    name = value;
                }//Fin de else.
                OnPropertyChanged("Name");
            }//Fin de set.
        }//Fin de propiedad Name.

        public string LastName
        {
            get
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    return this.Items[this.SelectedIndexOfCollection].LastName;
                }//Fin de if.
                else
                {
                    return lastName;
                }//Fin de else.
            }//Fin de get.
            set
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    this.Items[this.SelectedIndexOfCollection].LastName = value;
                }//Fin de if.
                else
                {
                    lastName = value;
                }//Fin de else.
                OnPropertyChanged("LastName");
            }//Fin de set.
        }//Fin de propiedad LastName.
        public string Document
        {
            get
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    return this.Items[this.SelectedIndexOfCollection].Document;
                }//Fin de if.
                else
                {
                    return document;
                }//Fin de else.
            }//Fin de get.
            set
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    this.Items[this.SelectedIndexOfCollection].Document = value;
                }//Fin de if.
                else
                {
                    document = value;
                }//Fin de else.
                OnPropertyChanged("Document");
            }//Fin de set.
        }//Fin de propiedad Document.
        public string Address
        {
            get
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    return this.Items[this.SelectedIndexOfCollection].Address;
                }//Fin de if.
                else
                {
                    return address;
                }//Fin de else.
            }//Fin de get.
            set
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    this.Items[this.SelectedIndexOfCollection].Address = value;
                }//Fin de if.
                else
                {
                    address = value;
                }//Fin de else.
                OnPropertyChanged("Address");
            }//Fin de set.
        }//Fin de propiedad Address.

        public string Gender
        {
            get
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    return this.Items[this.SelectedIndexOfCollection].Gender;
                }//Fin de if.
                else
                {
                    return gender;
                }//Fin de else.
            }//Fin de get.
            set
            {
                if (this.SelectedIndexOfCollection > -1)
                {
                    this.Items[this.SelectedIndexOfCollection].Gender = value;
                }//Fin de if.
                else
                {
                    gender = value;
                }//Fin de else.
                OnPropertyChanged("Gender");
            }//Fin de set.
        }//Fin de propiedad Gender.
    
        #endregion

        #region ICommand
        public ICommand NewClientCommand
        {
            get
            {
                return newClientCommand;
            }//Fin de get.
            set
            {
                newClientCommand = value;
            }//Fin de set.
        }//Fin de propiedad LastName.

        public ICommand AddClientCommand
        {
            get
            {
                return addClientCommand;
            }//Fin de get.
            set
            {
                addClientCommand = value;
            }//Fin de set.
        }//Fin de propiedad LastName.
        public ICommand DelClientCommand
        {
            get
            {
                return delClientCommand;
            }//Fin de get.
            set
            {
                delClientCommand = value;
            }//Fin de set.
        }//Fin de propiedad LastName.

        public ICommand UpdateClientCommand
        {
            get
            {
                return updateClientCommand;
            }//Fin de get.
            set
            {
                updateClientCommand = value;
            }//Fin de set.
        }//Fin de propiedad LastName.
        #endregion

        #region Constructores
        public ClientViewModel()
        {
            updateClientCommand = new CommandBase(param => this.UpdateClient());
            AddClientCommand = new CommandBase(param => this.AddClient());
            NewClientCommand = new CommandBase(new Action<Object>(NewClient));
            DelClientCommand = new CommandBase(param => this.DelClient());
            ViewClient();

        }//Fin de constructor.
        #endregion

        #region Interface
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }//Fin de if.
        }//Fin de OnPropertyChanged.
        /*protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        */
        #endregion

        #region Metodos/Funciones
        private void ViewClient() {
            conector = new OleDbConnection(ConnectionString);
            OleDbCommand cmd = new OleDbCommand("select * from tbl_client", conector);
            adapter = new OleDbDataAdapter(cmd);
            //cmd.Connection = conector;
            //cmd.CommandText = "select * from tbl_client";
            conector.Open();
            reader = cmd.ExecuteReader();
            this.Clear();
            while (reader.Read())
            {
                Client vlClient = new Client();
                vlClient.Id = (int)reader["Id"];
                vlClient.Name = (string)reader["Nombre"];
                vlClient.LastName = (string)reader["Apellido"];
                vlClient.Document = (string)reader["DNI"];
                vlClient.Address = (string)reader["Domicilio"];
                vlClient.Gender = (string)reader["Genero"];
                vlClient.Birthdate = (DateTime)reader["FdeNac"];
                this.Add(vlClient);
            }
            conector.Close();
        }
        private void AddClient()
        {

            Client vlClient = new Client();
            vlClient.Id = Id;
            vlClient.Name = Name;
            vlClient.LastName = LastName;
            vlClient.Document = Document;
            vlClient.Address = Address;
            vlClient.Gender = Gender;
            vlClient.Birthdate = Birthdate;
            if (SelectedIndexOfCollection != -1){
                
                    OleDbCommand cmd = new OleDbCommand();
                    conector.Open();
                    cmd.Connection = conector;
                    cmd.CommandText = "update tbl_client set Nombre='" + vlClient.Name + "', Apellido='" + vlClient.LastName + "', DNI='" + vlClient.Document + "', Domicilio='" + vlClient.Address + "', Genero='" + vlClient.Gender + "', FdeNac='" + vlClient.Birthdate + "' where Id=" + vlClient.Id;
                    cmd.ExecuteNonQuery();
                    tra = conector.BeginTransaction();
                    cmd.Transaction = tra;
                    conector.Close();
            }else
            {
                if (vlClient.Name != "" || vlClient.LastName != "" || vlClient.Address != "") {
                    OleDbCommand cmd = new OleDbCommand();
                    conector.Open();
                    cmd.Connection = conector;
                    cmd.CommandText = "insert into tbl_client (Nombre,Apellido,DNI,Domicilio,Genero,FdeNac) Values ('" + Name + "','" + LastName + "','" + Document + "','" + Address + "','" + Gender + "','" + Birthdate + "')";
                    cmd.ExecuteNonQuery();
                    tra = conector.BeginTransaction();
                    cmd.Transaction = tra;
                    conector.Close();
                }else
                {
                    MessageBox.Show("Debe Completar todos los Campos", "Campos Vacíos");
                }
            }
            ViewClient();
        }//Fin de AddClient.

        private void UpdateClient() { }
        private void NewClient(object obj)
        {
            SelectedIndexOfCollection = -1;
            Id = 0;
            Name = "";
            LastName = "";
            Document = "";
            Address = "";
            Gender = "";
            Birthdate = DateTime.Today;

        }//Fin de AddClient.
        private void DelClient()
        {

            Client vlClient = new Client();
            vlClient.Id = Id;
            vlClient.Name = Name;
            vlClient.LastName = LastName;
            vlClient.Document = Document;
            vlClient.Address = Address;
            vlClient.Gender = Gender;
            vlClient.Birthdate = Birthdate;
            OleDbCommand cmd = new OleDbCommand();
            if (vlClient.Id == 0)
            {
                MessageBox.Show("Por favor seleccione el registro a Eliminar", "Eliminar Registro");
            }
            else
            {
                if (MessageBox.Show("¿Seguro que desea eliminar el contacto seleccionado?",
                        "Confirmar eliminacion de registro", MessageBoxButton.YesNo,
                        MessageBoxImage.Warning) == MessageBoxResult.Yes) // Si al mostrar el cuadro de diálogo el usuario presiona el botón "yes" ...
                {
                    conector.Open();
                    cmd.Connection = conector;
                    cmd.CommandText = "delete from tbl_client where Id=" + vlClient.Id;
                    cmd.ExecuteNonQuery();
                    //OleDbTransaction tra= new OleDbTransaction(cmd);
                    tra = conector.BeginTransaction();
                    cmd.Transaction = tra;
                    conector.Close();
                }
            }
            ViewClient();
        }//Fin de AddClient.
        #endregion
    }//Fin de clase.
}
