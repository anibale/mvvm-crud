﻿using System;

namespace AppWPF.MVVM.Model
{
    class Client : NotifyBase
    {

        #region Atributos
        private int id;
        private string name;
        private string lastname;
        private string address;
        private string document;
        private string gender;
        private DateTime birthdate;
        #endregion

        #region Propiedades
        public int Id
        {
            get
            {
                return id;
            }//Fin de get.
            set
            {
                id = value;
                OnPropertyChanged("Id");
               // OnPropertyChanged("DisplayName");
            }//Fin de set.
        }//Fin de propiedad Id.

        public string Name
        {
            get
            {
                return name;
            }//Fin de get.
            set
            {
                name = value;
                OnPropertyChanged("Name");
               // OnPropertyChanged("DisplayName");
            }//Fin de set.
        }//Fin de propiedad Name.

        public string LastName
        {
            get
            {
                return lastname;
            }//Fin de get.
            set
            {
                lastname = value;
                OnPropertyChanged("LastName");
               //OnPropertyChanged("DisplayName");
            }//Fin de set.
        }//Fin de propiedad LastName.
        public string Document
        {
            get
            {
                return document;
            }

            set
            {
                document = value;
                OnPropertyChanged("Document");
            }
        }//Fin de propiedad Document
        public string Address
        {
            get
            {
                return address;
            }//Fin de get.
            set
            {
                address = value;
                OnPropertyChanged("Address");
                //OnPropertyChanged("DisplayName");
            }//Fin de set.
        }//Fin de propiedad Address.


        public string Gender
        {
            get
            {
                return gender;
            }

            set
            {
                gender = value;
                OnPropertyChanged("Gender");
            }
        }//Fin de propiedad Gender.
        public DateTime Birthdate
        {
            get
            {
                return birthdate;
            }

            set
            {
                birthdate = value;
                OnPropertyChanged("Birthdate");
            }
        }//Fin de propiedad Birthdate.


        #endregion

    }//Fin de clase.
}
